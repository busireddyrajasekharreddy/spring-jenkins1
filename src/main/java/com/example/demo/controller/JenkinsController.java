package com.example.demo.controller;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class JenkinsController {

	Logger logger = LoggerFactory.getLogger(JenkinsController.class);

	@PostConstruct
	public void init() {
		logger.info("inside init ..");
	}

	@GetMapping("/test")
	public String test() {
		logger.info("inside test................");

		return "test second commit........";
	}
}
